package ru.trifonov.tm;

import ru.trifonov.tm.bootstrap.*;

public final class ApplicationClient {
    public static void main(String[] args) {
        new Bootstrap().start();
    }
}