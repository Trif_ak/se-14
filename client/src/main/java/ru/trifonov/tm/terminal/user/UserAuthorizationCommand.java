package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Exception_Exception;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class UserAuthorizationCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": authorization in program";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[USER AUTHORIZATION]");
        @Nullable SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession != null) throw new NullPointerException("LOG OUT to LOG IN");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = bootstrap.getTerminalService().getInCommand();
        currentSession = bootstrap.getSessionEndpoint().openSession(login, password);
        if (currentSession == null) throw new NullPointerException("Not found user. Please, registration");
        bootstrap.setCurrentSession(currentSession);
        System.out.println("[OK]");
    }
}
