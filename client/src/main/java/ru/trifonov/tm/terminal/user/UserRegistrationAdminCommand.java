package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class UserRegistrationAdminCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-regAdmin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": registration new admin";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ADMIN REGISTRATION]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = bootstrap.getTerminalService().getInCommand();
        bootstrap.getUserEndpoint().registrationAdmin(currentSession, login, password);
        System.out.println("[OK]");
    }
}
