package ru.trifonov.tm.terminal.system;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class HelpCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": show all COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW ALL COMMANDS]");
        for (@NotNull final AbstractCommand command : bootstrap.getTerminalService().getCommands().values())
            System.out.printf("%-25s > %s \n", command.getName(), command.getDescription());
    }
}
