package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class UserRemoveOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE USER]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the user you want to removeOne");
        @Nullable final String id = bootstrap.getTerminalService().getInCommand();
        bootstrap.getUserEndpoint().deleteUser(currentSession, id);
        System.out.println("[OK]");
    }
}
