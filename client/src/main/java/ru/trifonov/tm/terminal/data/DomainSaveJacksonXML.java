package ru.trifonov.tm.terminal.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class DomainSaveJacksonXML extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "domain-saveJackXML";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": save domain to xml with jackson";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN SAVE OF JACKSON XML]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getDomainEndpoint().domainSaveJacksonXML(currentSession);
        System.out.println("[OK]");
    }
}
