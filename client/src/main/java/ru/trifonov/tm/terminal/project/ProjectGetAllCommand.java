package ru.trifonov.tm.terminal.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Project;
import ru.trifonov.tm.endpoint.ProjectDTO;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;


import java.util.Collection;
import java.util.List;

public final class ProjectGetAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL PROJECTS]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @NotNull final List<ProjectDTO> inputList = bootstrap.getProjectEndpoint().getAllProjectOfUser(currentSession);
        for (@NotNull final ProjectDTO project : inputList) {
            System.out.print("  NAME PROJECT " + project.getName());
            System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
            System.out.println("  ID PROJECT " + project.getId());
        }
        System.out.println("[OK]");
    }
}
