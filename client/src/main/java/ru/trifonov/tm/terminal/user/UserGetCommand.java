package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.endpoint.User;
import ru.trifonov.tm.endpoint.UserDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class UserGetCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET USER]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @NotNull final UserDTO userDTO = bootstrap.getUserEndpoint().getUser(currentSession);
        System.out.print("  LOGIN USER " + userDTO.getLogin());
        System.out.print("  PASSWORD USER " + userDTO.getPasswordHash());
        System.out.println("  ID USER " + userDTO.getId());
        System.out.println("[OK]");
    }
}
