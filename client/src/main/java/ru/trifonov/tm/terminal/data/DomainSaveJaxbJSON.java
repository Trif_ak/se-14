package ru.trifonov.tm.terminal.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class DomainSaveJaxbJSON extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "domain-saveJaxJS";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": save domain to json with jax-b";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN SAVE OF JAXB JSON]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getDomainEndpoint().domainSaveJaxbJSON(currentSession);
        System.out.println("[OK]");
    }
}
