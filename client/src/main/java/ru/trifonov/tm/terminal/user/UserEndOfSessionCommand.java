package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class UserEndOfSessionCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": end of user session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGOUT]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Log in to log out");
        bootstrap.getSessionEndpoint().closeSession(currentSession);
        bootstrap.setCurrentSession(null);
        System.out.println("[OK]");
    }
}
