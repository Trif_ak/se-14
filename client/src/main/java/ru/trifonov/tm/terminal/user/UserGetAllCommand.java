package ru.trifonov.tm.terminal.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.endpoint.User;
import ru.trifonov.tm.endpoint.UserDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

import java.util.Collection;

public final class UserGetAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL USERS]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @NotNull final Collection<UserDTO> inputList = bootstrap.getUserEndpoint().getAllUser(currentSession);
        for (@NotNull final UserDTO userDTO : inputList) {
            System.out.print("  LOGIN USER " + userDTO.getLogin());
            System.out.print("  PASSWORD USER " + userDTO.getPasswordHash());
            System.out.println("  ID USER " + userDTO.getId());
        }
        System.out.println("[OK]");
    }
}
