package ru.trifonov.tm.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

@Getter
@Setter
@NoArgsConstructor
public class TerminalService {
    @NotNull private final Scanner inCommand = new Scanner(System.in);
    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public String getInCommand() {
        return inCommand.nextLine();
    }
}
