package ru.trifonov.tm.terminal.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class DomainLoadJaxbXML extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "domain-loadJaxXML";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": load domain to xml with jax-b";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN LOAD OF JAXB XML]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getDomainEndpoint().domainLoadJaxbXML(currentSession);
        System.out.println("[OK]");
    }
}
