package ru.trifonov.tm.terminal.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class DomainSerializable extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "domain-ser";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": serializable domain";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN SERIALIZABLE]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        bootstrap.getDomainEndpoint().domainDeserializable(currentSession);
        System.out.println("[OK]");
    }
}