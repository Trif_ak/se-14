package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class TaskRemoveOneCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK DELETE]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the task you want to delete");
        @Nullable final String id = bootstrap.getTerminalService().getInCommand();
        bootstrap.getTaskEndpoint().getTask(currentSession, id);
        System.out.println("[OK]");
    }
}
