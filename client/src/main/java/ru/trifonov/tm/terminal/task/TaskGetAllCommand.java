package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.endpoint.Task;
import ru.trifonov.tm.endpoint.TaskDTO;
import ru.trifonov.tm.terminal.AbstractCommand;


import java.util.Collection;

public final class TaskGetAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": get all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL TASKS]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = bootstrap.getTerminalService().getInCommand();
        @NotNull final Collection<TaskDTO> inputList =
                bootstrap.getTaskEndpoint().getAllTaskOfProject(currentSession, projectId);
        for (@NotNull final TaskDTO task : inputList) {
            System.out.print("  NAME TASK " + task.getName());
            System.out.print("  DESCRIPTION TASK " + task.getDescription());
            System.out.println("  ID TASK " + task.getId());
        }
        System.out.println("[OK]");
    }
}
