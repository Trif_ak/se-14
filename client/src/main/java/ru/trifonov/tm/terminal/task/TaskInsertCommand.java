package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

public final class TaskInsertCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-insert";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK INSERT]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter name task");
        @Nullable final String name = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter description");
        @Nullable final String description = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter start date task. Date format DD.MM.YYYY");
        @Nullable final String beginDate = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter finish date task. Date format DD.MM.YYYY");
        @Nullable final String endDate = bootstrap.getTerminalService().getInCommand();
        bootstrap.getTaskEndpoint().initTask(currentSession, name, projectId, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
