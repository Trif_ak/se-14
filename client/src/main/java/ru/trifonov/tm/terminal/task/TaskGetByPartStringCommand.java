package ru.trifonov.tm.terminal.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.endpoint.Task;
import ru.trifonov.tm.endpoint.TaskDTO;
import ru.trifonov.tm.terminal.AbstractCommand;

import java.util.List;

public final class TaskGetByPartStringCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "task-findPart";
    }

    @Override
    public @NotNull String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT BY PART TITLE OR DESCRIPTION]");
        @Nullable final SessionDTO currentSession = bootstrap.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = bootstrap.getTerminalService().getInCommand();
        System.out.println("Enter part of title or description");
        @Nullable final String partString = bootstrap.getTerminalService().getInCommand();
        @NotNull final List<TaskDTO> tasks =
                bootstrap.getTaskEndpoint().getTaskByPartString(currentSession, projectId, partString);
        for (@NotNull final TaskDTO task : tasks) {
            System.out.print("  NAME TASK " + task.getName());
            System.out.print("  DESCRIPTION TASK " + task.getDescription());
            System.out.println("  ID TASK " + task.getId());
        }
        System.out.println("[OK]");
    }
}
