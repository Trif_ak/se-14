package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.trifonov.tm.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {
    public SessionEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        return serviceLocator.getSessionService().openSession(login, password);
    }

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getSessionService().closeSession(sessionDTO);
    }
}