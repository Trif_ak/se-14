package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.dto.UserDTO;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.trifonov.tm.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {
    public UserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void persistUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "user", partName = "user") @NotNull final User user
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getUserService().persist(user);
    }

//    @Override
//    @WebMethod
//    public User authorizationUser(
//            @WebParam(name = "login", partName = "login") @NotNull final String login,
//            @WebParam(name = "password", partName = "password") @NotNull final String password
//    ) throws Exception {
//        return serviceLocator.getUserService().existsUser(login, password);
//    }

    @Override
    @WebMethod
    public void registrationUser(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        serviceLocator.getUserService().registrationUser(login, password);
    }

    @Override
    @WebMethod
    public void registrationAdmin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getUserService().registrationAdmin(login, password);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "userId", partName = "userId") @NotNull final String userId,
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        @NotNull final String id = session.getUserId();
        @NotNull final RoleType roleType = session.getRole();
        serviceLocator.getUserService().update(id, login, password, roleType);
    }

    @Override
    @WebMethod
    public List<UserDTO> getAllUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        @NotNull final List<User> users = serviceLocator.getUserService().getAll();
        @NotNull final List<UserDTO> usersDTO = new ArrayList<>();
        for (@NotNull final User user : users) {
            usersDTO.add(serviceLocator.getUserService().entityToDTO(user));
        }
        return usersDTO;
    }

    @Override
    @WebMethod
    public UserDTO getUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String id = session.getUserId();
        @NotNull final User user = serviceLocator.getUserService().get(id);
        return serviceLocator.getUserService().entityToDTO(user);
    }

    @Override
    @WebMethod
    public void deleteUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        serviceLocator.getUserService().delete(id);
    }

    @Override
    public void changePassword(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "newPassword", partName = "newPassword") @NotNull String newPassword
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String id = session.getUserId();
        serviceLocator.getUserService().changePassword(id, newPassword);
    }
}