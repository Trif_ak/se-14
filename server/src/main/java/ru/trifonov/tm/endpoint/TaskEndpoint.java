package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.dto.TaskDTO;
import ru.trifonov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.trifonov.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {
    public TaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void initTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().insert(name, projectId, userId, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public void updateTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception, ParseException {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().update(name, id, projectId, userId, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public TaskDTO getTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final Task task = serviceLocator.getTaskService().get(id, userId);
        return serviceLocator.getTaskService().entityToDTO(task);
    }

    @Override
    @WebMethod
    public List<TaskDTO> getAllTaskOfProject (
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().getOfProject(projectId, userId);
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(serviceLocator.getTaskService().entityToDTO(task));
        }
        return tasksDTO;
    }

    @Override
    @WebMethod
    public void deleteTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().delete(id, userId);
    }

    @Override
    @WebMethod
    public void deleteAllTaskOfProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().deleteOfProject(projectId, userId);
    }

    @Override
    @WebMethod
    public void deleteAllTaskOfUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        serviceLocator.getTaskService().deleteOfUser(userId);
    }

    @Override
    @WebMethod
    public List<TaskDTO> sortByTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull final String comparatorName
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().sortBy(projectId, userId, comparatorName);
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(serviceLocator.getTaskService().entityToDTO(task));
        }
        return tasksDTO;
    }

    @Override
    @WebMethod
    public List<TaskDTO> getTaskByPartString(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "partString", partName = "partString") @NotNull final String partString
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().getByPartString(userId, projectId, partString);
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(serviceLocator.getTaskService().entityToDTO(task));
        }
        return tasksDTO;
    }

    @Override
    @WebMethod
    public List<TaskDTO> getAllTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(session);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().getAll();
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(serviceLocator.getTaskService().entityToDTO(task));
        }
        return tasksDTO;
    }
}
