package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.ServiceLocator;
import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.trifonov.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {
    public ProjectEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }
    @Override
    @WebMethod
    public void insertProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        serviceLocator.getProjectService().insert(userId, name, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public void updateProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        serviceLocator.getProjectService().update(id, userId, name, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public ProjectDTO getProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Project project = serviceLocator.getProjectService().get(id, userId);
        return serviceLocator.getProjectService().entityToDTO(project);
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getAllProjectOfUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().getOfUser(userId);
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(serviceLocator.getProjectService().entityToDTO(project));
        }
        return projectsDTO;
    }

    @Override
    @WebMethod
    public void deleteProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        serviceLocator.getProjectService().delete(id, userId);
    }

    @Override
    @WebMethod
    public void deleteAllProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        serviceLocator.getProjectService().deleteAll(userId);
    }

    @Override
    @WebMethod
    public List<ProjectDTO> sortByProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull final String comparatorName
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().sortBy(userId, comparatorName);
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(serviceLocator.getProjectService().entityToDTO(project));
        }
        return projectsDTO;
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getProjectByPartString(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "partString", partName = "partString") @NotNull final String partString
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().getByPartString(userId, partString);;
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(serviceLocator.getProjectService().entityToDTO(project));
        }
        return projectsDTO;
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getAllProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validateAdmin(sessionDTO);
        @NotNull final List<Project> projects = serviceLocator.getProjectService().getAll();
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(serviceLocator.getProjectService().entityToDTO(project));
        }
        return projectsDTO;
    }
}