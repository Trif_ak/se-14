package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    @WebMethod
    void insertProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "description", partName = "description") @NotNull String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull String endDate
    ) throws Exception;

    @WebMethod
    void updateProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull String name,
            @WebParam(name = "id", partName = "id") @NotNull String id,
            @WebParam(name = "description", partName = "description") @NotNull String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull String endDate
    ) throws Exception;

    @WebMethod
    ProjectDTO getProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    List<ProjectDTO> getAllProjectOfUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception;

    @WebMethod
    void deleteProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;

    @WebMethod
    void deleteAllProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception;

    @WebMethod
    List<ProjectDTO> sortByProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull String comparatorName
    ) throws Exception;

    @WebMethod
    List<ProjectDTO> getProjectByPartString(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "partString", partName = "partString") @NotNull String partString
    ) throws Exception;

    @WebMethod
    List<ProjectDTO> getAllProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception;
}
