package ru.trifonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.dto.UserDTO;
import ru.trifonov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {
    @WebMethod
    void persistUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "user", partName = "user") @NotNull User user
    ) throws Exception;

//    @WebMethod
//    UserDTO authorizationUser(
//            @WebParam(name = "login", partName = "login") @NotNull String login,
//            @WebParam(name = "password", partName = "password") @NotNull String password
//    ) throws Exception;

    @WebMethod
    void registrationUser(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

    @WebMethod
    void registrationAdmin(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "userId", partName = "userId") @NotNull String userId,
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    List<UserDTO> getAllUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception;

    @WebMethod
    UserDTO getUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception;

    @WebMethod
    void deleteUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull String id
    ) throws Exception;


    void changePassword(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "newPassword", partName = "newPassword") @NotNull String newPassword
    ) throws Exception;
}
