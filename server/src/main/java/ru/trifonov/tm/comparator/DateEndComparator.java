package ru.trifonov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.dto.ComparableEntityDTO;

import java.util.Comparator;

public final class DateEndComparator implements Comparator<ComparableEntityDTO> {
    @Override
    public int compare(@NotNull final ComparableEntityDTO o1, @NotNull final ComparableEntityDTO o2) {
        return o1.getBeginDate().compareTo(o2.getBeginDate());
    }
}
