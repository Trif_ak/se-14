package ru.trifonov.tm;

import ru.trifonov.tm.bootstrap.*;

public final class ApplicationServer {
    public static void main(String[] args) {
        new Bootstrap().start();
    }
}