package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ISessionRepository;
import ru.trifonov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository implements ISessionRepository {
    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull Session session) {
        entityManager.getTransaction().begin();
        entityManager.persist(session);
        entityManager.getTransaction().commit();
    }

    @Override
    public void delete(@NotNull String id) {
        @Nullable final Session session = getSession(id);
        entityManager.getTransaction().begin();
        entityManager.persist(session);
        entityManager.getTransaction().commit();
    }

    @Override
    public @Nullable Session getSession(@NotNull String id) {
        return entityManager
                .createQuery("SELECT s FROM Session WHERE s.id = ?1", Session.class)
                .setParameter(1, id)
                .getSingleResult();
    }

    @Override
    public @Nullable List<Session> getByUserId(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT s FROM Session s WHERE s.user.id = ?1", Session.class)
                .getResultList();
    }

    @Override
    public @Nullable List<Session> getAll() {
        return entityManager
                .createQuery("SELECT s FROM Session s", Session.class)
                .getResultList();
    }
}
