package ru.trifonov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.CurrentStatus;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "tasks")
public final class Task extends AbstractEntity {
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "status", nullable = false)
    private CurrentStatus status = CurrentStatus.PLANNED;

    @NotNull
    @Column(name = "begin_date", nullable = false)
    private Date beginDate = new Date();

    @NotNull
    @Column(name = "end_date", nullable = false)
    private Date endDate = new Date();

    @NotNull
    @Column(name = "create_date", nullable = false)
    private final Date createDate = new Date();

    public Task(
            @NotNull Project project, @NotNull User user, @NotNull String name,
            @NotNull String description, @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.project = project;
        this.user = user;
        this.name = name;
        this.description = description;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return " ID " + id +
                "  PROJECT_ID " + project.getId() +
                "  NAME " + name +
                "  DESCRIPTION " + description +
                "  STATUS " + status +
                "  TASK CREATE DATE " + dateFormat.format(createDate) +
                "  TASK BEGIN DATE " + dateFormat.format(beginDate) +
                "  TASK END DATE " + dateFormat.format(endDate);
    }
}
