package ru.trifonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@XmlRootElement(name = "user")
public final class UserDTO extends AbstractDTO implements Serializable {
    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private RoleType roleType = RoleType.REGULAR_USER;

    public UserDTO (@NotNull String id, @NotNull String login, @NotNull String passwordHash, @NotNull RoleType roleType) {
        this.id = id;
        this.login = login;
        this.passwordHash = passwordHash;
        this.roleType = roleType;
    }

    @NotNull
    @Override
    public String toString() {
        return " ID " + id +
                "  LOGIN " + login +
                "  PASSWORD_HASH " + passwordHash +
                "  ROLE_TYPE " + roleType;
    }
}