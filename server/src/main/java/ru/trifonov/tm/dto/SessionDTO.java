package ru.trifonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlRootElement;


@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "session")
public final class SessionDTO extends AbstractDTO implements Cloneable {
    @NotNull
    private String userId = "";

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    private String signature;

    @NotNull
    private RoleType role = RoleType.REGULAR_USER;

    public SessionDTO(@NotNull final String id, @NotNull String userId, @NotNull Long timestamp, @Nullable String signature, @NotNull RoleType role) {
        this.id = id;
        this.userId = userId;
        this.timestamp = timestamp;
        this.signature = signature;
        this.role = role;
    }

    @NotNull
    public SessionDTO clone() throws CloneNotSupportedException {
        return (SessionDTO) super.clone();
    }

    @NotNull
    @Override
    public String toString() {
        return " ID " + id +
                "  USER_ID " + userId +
                "  SIGNATURE " + signature +
                "  TIMESTAMP " + timestamp +
                "  ROLE " + role;
    }
}