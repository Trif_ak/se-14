package ru.trifonov.tm.enumerate;

import org.jetbrains.annotations.NotNull;

public enum RoleType {
    ADMIN("admin"), REGULAR_USER("user");
    private String type;

    @NotNull
    RoleType(@NotNull String type) {
        this.type = type;
    }

    public String getRoleType() {
        return type;
    }
}
