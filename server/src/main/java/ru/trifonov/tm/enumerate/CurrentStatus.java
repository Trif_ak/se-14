package ru.trifonov.tm.enumerate;

import org.jetbrains.annotations.NotNull;

public enum CurrentStatus {
    PLANNED("planned"), PROCESS("in process"), COMPLETED("completed");
    @NotNull private String status;

    CurrentStatus(@NotNull String status) {
        this.status = status;
    }

    public String getCurrentStatus() {
        return status;
    }
}
