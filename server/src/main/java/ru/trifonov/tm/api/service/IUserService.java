package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.dto.UserDTO;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.List;

public interface IUserService {
    void persist(@Nullable User user);
    void merge(@Nullable User user);
    void registrationUser(@Nullable String login, @Nullable String password);
    void registrationAdmin(@Nullable String login, @Nullable String password);
    void addUser() throws Exception;
    void update(@Nullable String id, @Nullable String login, @Nullable String password, @Nullable RoleType roleType);
    void changePassword(@Nullable String userId, @Nullable String newPassword);
    @NotNull List<User> getAll();
    @NotNull User get(@Nullable String id);
    boolean getByLogin(@NotNull String login);
    void delete(@Nullable String id);
    @NotNull User existsUser(@Nullable String login, @Nullable String password);
    void load(@Nullable List<User> users);

    @NotNull UserDTO entityToDTO(@NotNull User user);
}
