package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    void persist(@NotNull Task task);
    void merge(@NotNull Task task);
    @Nullable List<Task> getAll();
    @Nullable List<Task> getOfProject(@NotNull String projectId, @NotNull String userId);
    @Nullable List<Task> getOfUser(@NotNull String userId);
    @Nullable Task get(@NotNull String id, @NotNull String userId);
    void deleteOfUser(@NotNull String userId);
    void deleteOfProject(@NotNull String projectId, @NotNull String userId);
    void delete(@NotNull String id, @NotNull String userId);
}
