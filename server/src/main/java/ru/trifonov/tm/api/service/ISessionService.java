package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.entity.User;

import java.util.List;

public interface ISessionService {
    @NotNull SessionDTO sign(@Nullable SessionDTO sessionDTO);
    void validateAdmin(@Nullable SessionDTO sessionDTO) throws Exception;
    void validate(@Nullable SessionDTO sessionDTO) throws Exception;
    @NotNull SessionDTO openSession(@Nullable String login, @Nullable String password) throws Exception;
    void closeSession(@Nullable SessionDTO sessionDTO) throws Exception;
    void persist(@Nullable Session session) throws Exception;
    @NotNull List<SessionDTO> getByUserId(@NotNull SessionDTO sessionDTO) throws Exception;
    @NotNull List<Session> getAll() throws Exception;
    void load(@Nullable List<Session> sessions) throws Exception;
}
