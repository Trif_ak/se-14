package ru.trifonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.entity.Project;


import java.util.List;

public interface IProjectService {
    void persist(@Nullable Project project) throws Exception;
    void merge(@Nullable Project project);
    void insert(@Nullable String name, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    void update(@Nullable String name, @Nullable String id, @Nullable String userId, @Nullable String description, @Nullable String beginDate, @Nullable String endDate) throws Exception;
    Project get(@Nullable String id, @Nullable String userId) throws Exception;
    List<Project> getOfUser(@Nullable String userId) throws Exception;
    void delete(@Nullable String id, @Nullable String userId) throws Exception;
    void deleteAll(@Nullable String userId) throws Exception;
    List<Project> sortBy(@Nullable String userId, @Nullable String comparatorName) throws Exception;
    List<Project> getByPartString(@Nullable String userId, @Nullable String partString) throws Exception;
    List<Project> getAll() throws Exception;
    void load(@Nullable List<Project> projects) throws Exception;

    @NotNull ProjectDTO entityToDTO(@NotNull Project project);
}
