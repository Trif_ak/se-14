package ru.trifonov.tm.api;

import ru.trifonov.tm.api.service.*;

import javax.persistence.EntityManagerFactory;

public interface ServiceLocator {
    IProjectService getProjectService();
    ITaskService getTaskService();
    IUserService getUserService();
    ISessionService getSessionService();
    IDomainService getDomainService();
    IPropertyService getPropertyService();
    EntityManagerFactory getEntityManagerFactory();
}
