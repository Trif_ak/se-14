package ru.trifonov.tm.domain;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.dto.TaskDTO;
import ru.trifonov.tm.dto.UserDTO;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.entity.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@XmlType
@XmlRootElement(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Domain implements Serializable {
    @XmlElement(name = "users")
    @XmlElementWrapper(name = "users")
    @NotNull private List<User> users = new ArrayList<>();
    @XmlElement(name = "projects")
    @XmlElementWrapper(name = "projects")
    @NotNull private List<Project> projects = new ArrayList<>();
    @XmlElement(name = "tasks")
    @XmlElementWrapper(name = "tasks")
    @NotNull private List<Task> tasks = new ArrayList<>();
    @XmlElement(name = "sessions")
    @XmlElementWrapper(name = "sessions")
    @NotNull private List<Session> sessions = new ArrayList<>();
}
